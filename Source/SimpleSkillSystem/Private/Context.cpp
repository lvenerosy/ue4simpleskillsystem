/*
 * @license
 * Copyright 2020 Loïc Venerosy
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "Context.h"

#include "EffectComponents/EffectDuration.h"
#include "Systems/SystemTickDuration.h"
#include "Systems/SystemTryToStartNext.h"

struct UContext::Implementation final : public ImplementationDeleter
{
	TArray<USystem*> Systems;
	USystemTickDuration* SystemTickDuration;
	USystemTryToStartNext* SystemTryToStartNext;
	TArray<USystem::BufferedEffect> Effects;
	TMap<const UObject*, TArray<UEffect*>> Responses;
	bool IsWithinSystems = false;

	void AddReferencedObjects(FReferenceCollector& Collector) override;

	void UpdateBuffer();
	void ClearTerminated();
};

void UContext::Implementation::AddReferencedObjects(FReferenceCollector& Collector)
{
	TArray<UObject*> UnreferencedObjects;
	UnreferencedObjects.Append(Systems);
	UnreferencedObjects.Add(SystemTickDuration);
	UnreferencedObjects.Add(SystemTryToStartNext);
	for (auto& Effect : Effects)
	{
		UnreferencedObjects.Add(Effect.Current);
		UnreferencedObjects.Add(Effect.Next);
	}

	Collector.AddReferencedObjects(UnreferencedObjects);
}

void UContext::Implementation::UpdateBuffer()
{
	for (auto& Effect : Effects)
	{
		Effect.Current->CopyProperties(Effect.Next);
	}
}

void UContext::Implementation::ClearTerminated()
{
	Effects.RemoveAllSwap(
		[](const USystem::BufferedEffect& Effect) -> bool
	{
		return Effect.Next->IsTerminated();
	}
	);
}

UContext::UContext()
	:PointerToImplementation(MakeUnique<Implementation>()), Concrete(static_cast<Implementation*>(PointerToImplementation.Get()))
{
	Concrete->SystemTickDuration = CreateDefaultSubobject<USystemTickDuration>(TEXT("SystemTickDuration"));
	Concrete->SystemTryToStartNext = CreateDefaultSubobject<USystemTryToStartNext>(TEXT("SystemTryToStartNext"));
}

UEffect* UContext::CreateEffect()
{
	return NewObject<UEffect>(this);
}

bool UContext::IsRunning(const UEffect* Effect) const
{
	check(Effect != nullptr);
	check(Effect->GetOuter() == this);

	return Concrete->Effects.ContainsByPredicate(
		[&](const USystem::BufferedEffect& Buffered) -> bool
	{
		return Effect == Buffered.Current || Effect == Buffered.Next;
	}
	) && !Effect->IsTerminated();
}

void UContext::Launch(UEffect* Effect)
{
	check(Effect != nullptr);
	check(Effect->GetOuter() == this);
	check(!IsRunning(Effect));
	const TSet<TSubclassOf<UEffectComponent>> EffectDurationSet{ UEffectDuration::StaticClass() };
	checkf(Effect->Matches(EffectDurationSet), TEXT("Effects are expected to have an EffectDuration component."));

	UEffect* Current = NewObject<UEffect>(this);
	Current->CopyQualifiers(Effect);
	Current->CopyProperties(Effect);

	Concrete->Effects.Add({ Current, Effect });
}

UEffectComponent* UContext::AddQualifierPostStart(UEffect* Effect, TSubclassOf<UEffectComponent> NewQualifierType)
{
	check(Effect != nullptr);
	check(Effect->GetOuter() == this);
	check(IsRunning(Effect));
	check(NewQualifierType != nullptr);
	checkf(!IsWithinSystems(), TEXT("Use UEffect::AddQualifierPreStart or UEffect::AddQualifierPostStart instead"));

	USystem::BufferedEffect* Buffered = GetCorrespondingBuffered(Effect);
	check(Buffered != nullptr);

	Buffered->Current->UnsafelyAddQualifier(NewQualifierType);
	return Buffered->Next->UnsafelyAddQualifier(NewQualifierType);
}

void UContext::RemoveQualifierPostStart(UEffect* Effect, TSubclassOf<UEffectComponent> QualifierTypeToRemove)
{
	check(Effect != nullptr);
	check(Effect->GetOuter() == this);
	check(IsRunning(Effect));
	check(QualifierTypeToRemove != nullptr);
	checkf(!IsWithinSystems(), TEXT("Use UEffect::RemoveQualifierPreStart or UEffect::RemoveQualifierPostStart instead"));

	USystem::BufferedEffect* Buffered = GetCorrespondingBuffered(Effect);
	check(Buffered != nullptr);

	Buffered->Current->UnsafelyRemoveQualifier(QualifierTypeToRemove);
	Buffered->Next->UnsafelyRemoveQualifier(QualifierTypeToRemove);
}

void UContext::AddSystem(TSubclassOf<USystem> NewSystemType)
{
	check(NewSystemType != nullptr);
	checkf(NewSystemType != USystemTickDuration::StaticClass() && NewSystemType != USystemTryToStartNext::StaticClass(), TEXT("`USystemTickDuration` and `USystemTryToStartNext` are already running by default in order to enforce that the next effect does not start before the current one runs through all the systems."));

	Concrete->Systems.Add(NewObject<USystem>(this, NewSystemType));
}

void UContext::Tick(float DeltaTime)
{
	check(DeltaTime >= 0.f);

	Concrete->Responses.Empty(Concrete->Responses.Num());
	Concrete->ClearTerminated();
	Concrete->UpdateBuffer();

	Concrete->IsWithinSystems = true;
	for (auto EffectIterator = Concrete->Effects.CreateIterator(); EffectIterator; ++EffectIterator)
	{
		checkf(Concrete->SystemTickDuration->Matches(EffectIterator->Current), TEXT("Effects are expected to have an EffectDuration component."));
		Concrete->SystemTickDuration->Run(DeltaTime, *EffectIterator);

		for (auto& System : Concrete->Systems)
		{
			if (System->Matches(EffectIterator->Current))
			{
				System->Run(DeltaTime, *EffectIterator);
			}
		}

		if (Concrete->SystemTryToStartNext->Matches(EffectIterator->Current))
		{
			Concrete->SystemTryToStartNext->Run(DeltaTime, *EffectIterator);
		}

		// Termination happens in the same tick in case systems other than `USystemTickDuration` modify `EffectDuration`
		const TSet<TSubclassOf<UEffectComponent>> EffectDurationSet{ UEffectDuration::StaticClass() };
		UEffectDuration* EffectDuration = Cast<UEffectDuration>(EffectIterator->Next->GetMatching(EffectDurationSet)[UEffectDuration::StaticClass()]);
		if (EffectDuration->Duration <= 0.f && !EffectDuration->TimerIsPaused)
		{
			EffectIterator->Next->Terminate();
		}

		// Necessary for EffectPrevious to be able to access data that is up to date
		if (EffectIterator->Next->IsTerminated())
		{
			EffectIterator->Current->CopyProperties(EffectIterator->Next);
		}

		if (Concrete->Responses.Contains(EffectIterator->Next->RequestSender()))
		{
			Concrete->Responses[EffectIterator->Next->RequestSender()].Add(EffectIterator->Next);
		}
		else
		{
			Concrete->Responses.Add(EffectIterator->Next->RequestSender(), { EffectIterator->Next });
		}
	}
	Concrete->IsWithinSystems = false;
}

bool UContext::IsWithinSystems() const
{
	return Concrete->IsWithinSystems;
}

USystem::BufferedEffect* UContext::GetCorrespondingBuffered(const UEffect* Effect) const
{
	check(Effect != nullptr);
	check(Effect->GetOuter() == this);
	check(IsRunning(Effect));

	return Concrete->Effects.FindByPredicate(
		[&](const USystem::BufferedEffect& Buffered) -> bool
	{
		return Effect == Buffered.Current || Effect == Buffered.Next;
	}
	);
}

TArray<UEffect*> UContext::Query(const UObject* RequestSender) const
{
	if (Concrete->Responses.Contains(RequestSender))
	{
		return Concrete->Responses[RequestSender];
	}
	else
	{
		return {};
	}
}
