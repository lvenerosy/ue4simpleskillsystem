/*
 * @license
 * Copyright 2020 Loïc Venerosy
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "System.h"

#include "Context.h"

struct USystemTuples::Implementation final : public ImplementationDeleter
{
    bool Initialized = false;

    TMap<TSubclassOf<UEffectComponent>, UEffectComponent*> ReadOnlyParametersTuple;
    TMap<TSubclassOf<UEffectComponent>, UEffectComponent*> WriteOnlyResultTuple;

	void AddReferencedObjects(FReferenceCollector& Collector) override {}
};

USystemTuples::USystemTuples()
:PointerToImplementation(MakeUnique<Implementation>()), Concrete(static_cast<Implementation*>(PointerToImplementation.Get()))
{
}

void USystemTuples::Initialize(TMap<TSubclassOf<UEffectComponent>, UEffectComponent*> ReadOnlyParametersTuple, TMap<TSubclassOf<UEffectComponent>, UEffectComponent*> WriteOnlyResultTuple)
{
    check(!Concrete->Initialized);

    Concrete->Initialized = true;

    Concrete->ReadOnlyParametersTuple = ReadOnlyParametersTuple;
    Concrete->WriteOnlyResultTuple = WriteOnlyResultTuple;
}

UEffectComponent* USystemTuples::GetReadOnly(TSubclassOf<UEffectComponent> QualifierType) const
{
    check(QualifierType != nullptr);
    check(Concrete->ReadOnlyParametersTuple.Contains(QualifierType))

    return Concrete->ReadOnlyParametersTuple[QualifierType];
}

UEffectComponent* USystemTuples::GetWriteOnly(TSubclassOf<UEffectComponent> QualifierType) const
{
    check(QualifierType != nullptr);
    check(Concrete->WriteOnlyResultTuple.Contains(QualifierType))

    return Concrete->WriteOnlyResultTuple[QualifierType];
}

struct USystem::Implementation final : public ImplementationDeleter
{
    TSet<TSubclassOf<UEffectComponent>> QualifiersIds;

	void AddReferencedObjects(FReferenceCollector& Collector) override {}
};

USystem::USystem()
:PointerToImplementation(MakeUnique<Implementation>()), Concrete(static_cast<Implementation*>(PointerToImplementation.Get()))
{
}

void USystem::PostInitProperties()
{
    Super::PostInitProperties();
    
    OnInitialization();
}

void USystem::AddQualifierId(TSubclassOf<UEffectComponent> QualifierId)
{
    check(QualifierId != nullptr);
    
    Concrete->QualifiersIds.Add(QualifierId);
}

bool USystem::Matches(const UEffect* Effect) const
{
    check(Effect != nullptr);
    check(GetOuter() == Effect->GetOuter());

    return Effect->Matches(Concrete->QualifiersIds);
}

void USystem::Run(float DeltaTime, const BufferedEffect& Buffered)
{
	check(Buffered.Current != nullptr);
	check(Buffered.Next != nullptr);
	check(Cast<UContext>(GetOuter()) != nullptr);
	check(GetOuter() == Buffered.Current->GetOuter());
	check(GetOuter() == Buffered.Next->GetOuter());
	check(Matches(Buffered.Current));

	USystemTuples* SystemTuples = NewObject<USystemTuples>(this);
	SystemTuples->Initialize(Buffered.Current->GetMatching(Concrete->QualifiersIds), Buffered.Next->GetMatching(Concrete->QualifiersIds));

#if UE_BUILD_DEBUG || UE_BUILD_DEVELOPMENT
	OnPIEOrDevelopmentPreRun(DeltaTime, SystemTuples);
#else
	if (GetWorld()->WorldType == EWorldType::PIE)
	{
		OnPIEOrDevelopmentPreRun(DeltaTime, SystemTuples);
	}
#endif
	OnRun(DeltaTime, SystemTuples);
}

void USystem::OnInitialization_Implementation()
{
}

void USystem::OnPIEOrDevelopmentPreRun_Implementation(float DeltaTime, const USystemTuples* SystemTuples)
{
	check(DeltaTime >= 0.f);
	check(SystemTuples != nullptr);
	check(SystemTuples->GetOuter() == this);
}

void USystem::OnRun_Implementation(float DeltaTime, const USystemTuples* SystemTuples) const
{
    check(DeltaTime >= 0.f);
    check(SystemTuples != nullptr);
    check(SystemTuples->GetOuter() == this);
}
