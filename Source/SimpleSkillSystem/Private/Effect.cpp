/*
 * @license
 * Copyright 2020 Loïc Venerosy
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "Effect.h"

#include "Context.h"
#include "EffectComponents/EffectDuration.h"

struct UEffect::Implementation final : public ImplementationDeleter
{
	UObject* Sender;
	bool Terminated = false;

	TMap<TSubclassOf<UEffectComponent>, UEffectComponent*> Qualifiers;

	void AddReferencedObjects(FReferenceCollector& Collector) override;
};

void UEffect::Implementation::AddReferencedObjects(FReferenceCollector& Collector)
{
	TArray<UEffectComponent*> UnreferencedObjects;
	Qualifiers.GenerateValueArray(UnreferencedObjects);
	Collector.AddReferencedObjects(UnreferencedObjects);
}

UEffect::UEffect()
	:PointerToImplementation(MakeUnique<Implementation>()), Concrete(static_cast<Implementation*>(PointerToImplementation.Get()))
{
}

bool UEffect::IsRunning() const
{
	check(Cast<UContext>(GetOuter()) != nullptr);

	return Cast<UContext>(GetOuter())->IsRunning(this);
}

UEffectComponent* UEffect::UnsafelyAddQualifier(TSubclassOf<UEffectComponent> NewQualifierType)
{
	check(NewQualifierType != nullptr);
	check(!Concrete->Qualifiers.Contains(NewQualifierType));

	UEffectComponent* EffectComponent = NewObject<UEffectComponent>(this, NewQualifierType);

	Concrete->Qualifiers.Add(NewQualifierType, EffectComponent);

	return EffectComponent;
}

UEffectComponent* UEffect::AddQualifierPreStart(TSubclassOf<UEffectComponent> NewQualifierType)
{
	check(NewQualifierType != nullptr);
	check(!Concrete->Qualifiers.Contains(NewQualifierType));
	checkf(!IsRunning(), TEXT("UEffect::AddQualifierPostStart if you want to add a component while the effect is running"));

	return UnsafelyAddQualifier(NewQualifierType);
}

UEffectComponent* UEffect::AddQualifierPostStart(TSubclassOf<UEffectComponent> NewQualifierType)
{
	check(NewQualifierType != nullptr);
	check(!Concrete->Qualifiers.Contains(NewQualifierType));
	checkf(IsRunning(), TEXT("UEffect::AddQualifierPreStart if you want to add a component before the effect has started"));

	UContext* Context = Cast<UContext>(GetOuter());
	check(Context != nullptr);
	checkf(!Context->IsWithinSystems(), TEXT("You cannot add qualifiers from a running effect while within a system"));

	return Context->AddQualifierPostStart(this, NewQualifierType);
}

void UEffect::UnsafelyRemoveQualifier(TSubclassOf<UEffectComponent> QualifierTypeToRemove)
{
	check(QualifierTypeToRemove != nullptr);
	check(Concrete->Qualifiers.Contains(QualifierTypeToRemove));

	Concrete->Qualifiers.Remove(QualifierTypeToRemove);
}

void UEffect::RemoveQualifierPreStart(TSubclassOf<UEffectComponent> QualifierTypeToRemove)
{
	check(QualifierTypeToRemove != nullptr);
	check(Concrete->Qualifiers.Contains(QualifierTypeToRemove));
	checkf(!IsRunning(), TEXT("UEffect::RemoveQualifierPostStart if you want to remove a component while the effect is running"));

	UnsafelyRemoveQualifier(QualifierTypeToRemove);
}

void UEffect::RemoveQualifierPostStart(TSubclassOf<UEffectComponent> QualifierTypeToRemove)
{
	check(QualifierTypeToRemove != nullptr);
	check(Concrete->Qualifiers.Contains(QualifierTypeToRemove));
	checkf(IsRunning(), TEXT("UEffect::RemoveQualifierPreStart if you want to remove a component before the effect has started"));

	UContext* Context = Cast<UContext>(GetOuter());
	check(Context != nullptr);
	checkf(!Context->IsWithinSystems(), TEXT("You cannot remove qualifiers from a running effect while within a system"));

	Context->RemoveQualifierPostStart(this, QualifierTypeToRemove);
}

bool UEffect::Matches(const TSet<TSubclassOf<UEffectComponent>>& SystemQualifiersIds) const
{
	for (auto& QualifierId : SystemQualifiersIds)
	{
		if (!Concrete->Qualifiers.Contains(QualifierId))
		{
			return false;
		}
	}

	return true;
}

TMap<TSubclassOf<UEffectComponent>, UEffectComponent*> UEffect::GetMatching(const TSet<TSubclassOf<UEffectComponent>>& SystemQualifiersIds) const
{
	check(Matches(SystemQualifiersIds));

	TMap<TSubclassOf<UEffectComponent>, UEffectComponent*> SystemQualifiers;
	SystemQualifiers.Reserve(SystemQualifiersIds.Num());

	for (auto& SystemQualifierId : SystemQualifiersIds)
	{
		SystemQualifiers.Add(SystemQualifierId, Concrete->Qualifiers[SystemQualifierId]);
	}

	return SystemQualifiers;
}

void UEffect::CopyQualifiers(const UEffect* Source)
{
	check(Source != nullptr);

	for (auto& Qualifier : Source->Concrete->Qualifiers)
	{
		AddQualifierPreStart(Qualifier.Key);
	}
}

static void CopyUProperties(UObject* Destination, UObject* Source)
{
	check(Destination != nullptr);
	check(Source != nullptr);
	check(Destination->StaticClass() == Source->StaticClass());

	for (TFieldIterator<FProperty> PropertyIterator(Destination->GetClass()); PropertyIterator; ++PropertyIterator)
	{
		FProperty* SourceProperty = Source->GetClass()->FindPropertyByName(FName(*PropertyIterator->GetName()));

		PropertyIterator->CopyCompleteValue(PropertyIterator->ContainerPtrToValuePtr<void>(Destination), SourceProperty->ContainerPtrToValuePtr<void>(Source));
	}
}

void UEffect::CopyProperties(const UEffect* Source)
{
	check(Source != nullptr);
	check(StaticClass() == Source->StaticClass());

	Concrete->Sender = Source->Concrete->Sender;
	Concrete->Terminated = Source->Concrete->Terminated;

	for (auto& Component : Concrete->Qualifiers)
	{
		CopyUProperties(Component.Value, Source->Concrete->Qualifiers[Component.Key]);
	}
}

void UEffect::Start(UObject* RequestSender, const float DeltaTimeSlack)
{
	check(Cast<UContext>(GetOuter()) != nullptr);
	check(!IsRunning());
	check(RequestSender != nullptr);
	check(DeltaTimeSlack >= 0.f);
	checkf(Concrete->Qualifiers.Contains(UEffectDuration::StaticClass()), TEXT("Effects are expected to have an EffectDuration component."));

	Concrete->Sender = RequestSender;
	UEffectDuration* EffectDuration = Cast<UEffectDuration>(Concrete->Qualifiers[UEffectDuration::StaticClass()]);
	check(EffectDuration != nullptr);
	EffectDuration->Duration -= DeltaTimeSlack;

	Cast<UContext>(GetOuter())->Launch(this);
}

void UEffect::StartNext(UEffect* Previous, const float DeltaTimeSlack)
{
	Start(Previous->Concrete->Sender, DeltaTimeSlack);
}

const UObject* UEffect::RequestSender() const
{
	return Concrete->Sender;
}

bool UEffect::IsTerminated() const
{
	return Concrete->Terminated;
}

void UEffect::Terminate()
{
	check(!IsTerminated());
	check(IsRunning());
	check(Cast<UContext>(GetOuter()) != nullptr);

	Concrete->Terminated = true;
}
