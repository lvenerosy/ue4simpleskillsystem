/*
 * @license
 * Copyright 2020 Loïc Venerosy
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "EffectComponents/EffectPrevious.h"

#include "Context.h"

UEffectComponent* UEffectPrevious::GetReadOnly(TSubclassOf<UEffectComponent> QualifierType) const
{
	check(Previous != nullptr);
	checkf(Previous->IsRunning(), TEXT("Usable only during the tick within which `Previous` is terminating and the new effect is starting. You may want to use `UEffectNext::GetWriteOnly` instead."));

	const UContext* Context = Cast<UContext>(Previous->GetOuter());
	check(Context != nullptr);

	return Context->GetCorrespondingBuffered(Previous)->Current->GetMatching({ QualifierType })[QualifierType];
}
