/*
 * @license
 * Copyright 2020 Loïc Venerosy
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "Systems/SystemTickDuration.h"

#include "EffectComponents/EffectDuration.h"

void USystemTickDuration::OnInitialization_Implementation()
{
	Super::OnInitialization_Implementation();

	AddQualifierId(UEffectDuration::StaticClass());
}

void USystemTickDuration::OnRun_Implementation(float DeltaTime, const USystemTuples* SystemTuples) const
{
	Super::OnRun_Implementation(DeltaTime, SystemTuples);

	UClass* DurationClass = UEffectDuration::StaticClass();

	check(SystemTuples->GetReadOnly(DurationClass) != nullptr);
	const UEffectDuration* ReadOnlyDuration = Cast<UEffectDuration>(SystemTuples->GetReadOnly(DurationClass));
	check(ReadOnlyDuration != nullptr);

	if (!ReadOnlyDuration->TimerIsPaused)
	{
		check(SystemTuples->GetWriteOnly(DurationClass) != nullptr);
		UEffectDuration* WriteOnlyDuration = Cast<UEffectDuration>(SystemTuples->GetWriteOnly(DurationClass));
		check(WriteOnlyDuration != nullptr);

		WriteOnlyDuration->Duration = ReadOnlyDuration->Duration - DeltaTime;
	}
}
