/*
 * @license
 * Copyright 2020 Loïc Venerosy
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "Systems/SystemTryToStartNext.h"

#include "EffectComponents/EffectDuration.h"
#include "EffectComponents/EffectNext.h"
#include "EffectComponents/EffectPrevious.h"

void USystemTryToStartNext::OnInitialization_Implementation()
{
	Super::OnInitialization_Implementation();

	AddQualifierId(UEffectDuration::StaticClass());
	AddQualifierId(UEffectNext::StaticClass());
}

void USystemTryToStartNext::OnRun_Implementation(float DeltaTime, const USystemTuples* SystemTuples) const
{
	Super::OnRun_Implementation(DeltaTime, SystemTuples);

	UClass* DurationClass = UEffectDuration::StaticClass();
	check(SystemTuples->GetWriteOnly(DurationClass) != nullptr);

	// Inside `USystemTryToStartNext` it is ok to read from `WriteOnlyResultTuple` as the system is hard coded to run after every other system
	const UEffectDuration* EffectDuration = Cast<UEffectDuration>(SystemTuples->GetWriteOnly(DurationClass));
	check(EffectDuration != nullptr);

	if (!EffectDuration->TimerIsPaused)
	{
		const float DeltaTimeSlack = -EffectDuration->Duration;
		if (DeltaTimeSlack >= 0.f)
		{
			UClass* NextClass = UEffectNext::StaticClass();
			check(SystemTuples->GetWriteOnly(NextClass) != nullptr);

			UEffectNext* EffectNext = Cast<UEffectNext>(SystemTuples->GetWriteOnly(NextClass));
			check(EffectNext != nullptr);
			check(EffectNext->Next != nullptr);

			UEffectPrevious* Previous = Cast<UEffectPrevious>(EffectNext->Next->AddQualifierPreStart(UEffectPrevious::StaticClass()));
			check(Previous != nullptr);

			UEffect* CurrentEffect = Cast<UEffect>(EffectNext->GetOuter());
			check(CurrentEffect != nullptr);

			Previous->Previous = CurrentEffect;

			UEffect* Next = EffectNext->Next;
			check(Next != nullptr);

			Next->StartNext(CurrentEffect, DeltaTimeSlack);
		}
	}
}
