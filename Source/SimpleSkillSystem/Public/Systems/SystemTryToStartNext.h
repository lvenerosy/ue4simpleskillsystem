/*
 * @license
 * Copyright 2020 Loïc Venerosy
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "SimpleSkillSystemCore.h"

#include "../System.h"

#include "SystemTryToStartNext.generated.h"

UCLASS(BlueprintType)
	class SIMPLESKILLSYSTEM_API USystemTryToStartNext final : public USystem
{
	GENERATED_BODY()

protected:
	void OnInitialization_Implementation() override;

	void OnRun_Implementation(float DeltaTime, const USystemTuples* SystemTuples) const override;
};
