/*
 * @license
 * Copyright 2020 Loïc Venerosy
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "SimpleSkillSystemCore.h"

#include "Effect.h"
#include "EffectComponent.h"

#include "System.generated.h"

// besides `USystemTickDuration` and `USystemTryToStartNext`, the execution order for the systems is unknown, thus read operations are invalid on `WriteOnlyResultTuple`
// in case you have systems that require a particular order, there most likely is a strong coupling so they should live in the same system
UCLASS(BlueprintType)
	class SIMPLESKILLSYSTEM_API USystemTuples final : public UObject
{
	GENERATED_BODY()

	struct Implementation;
	TUniquePtr<ImplementationDeleter> PointerToImplementation;

	Implementation* Concrete;

public:
	USystemTuples();

	void Initialize(TMap<TSubclassOf<UEffectComponent>, UEffectComponent*> ReadOnlyParametersTuple, TMap<TSubclassOf<UEffectComponent>, UEffectComponent*> WriteOnlyResultTuple);

	UFUNCTION(BlueprintCallable, Category = "Skill", meta = (DeterminesOutputType = "QualifierType"))
	UEffectComponent* GetReadOnly(TSubclassOf<UEffectComponent> QualifierType) const;

	UFUNCTION(BlueprintCallable, Category = "Skill", meta = (DeterminesOutputType = "QualifierType"))
	UEffectComponent* GetWriteOnly(TSubclassOf<UEffectComponent> QualifierType) const;
};

UCLASS(Blueprintable, BlueprintType, Abstract)
	class SIMPLESKILLSYSTEM_API USystem : public UObject
{
	GENERATED_BODY()

		struct Implementation;
	TUniquePtr<ImplementationDeleter> PointerToImplementation;

	Implementation* Concrete;

public:
	struct BufferedEffect
	{
		UEffect* Current;
		UEffect* Next;
	};

	USystem();

	void PostInitProperties() override;

	UFUNCTION(BlueprintCallable, Category = "Skill")
		void AddQualifierId(TSubclassOf<UEffectComponent> QualifierId);

	bool Matches(const UEffect* Effect) const;

	// In ECS, systems have no state and should be const
	// `Run` is non-const but the shipping implementation `OnRun` is
	void Run(float DeltaTime, const BufferedEffect& Buffered);

protected:
	UFUNCTION(BlueprintNativeEvent, Category = "Skill")
		void OnInitialization();

	UFUNCTION(BlueprintNativeEvent, Category = "Skill")
		void OnPIEOrDevelopmentPreRun(float DeltaTime, const USystemTuples* SystemTuples);

	UFUNCTION(BlueprintNativeEvent, Category = "Skill")
		void OnRun(float DeltaTime, const USystemTuples* SystemTuples) const;

	virtual ~USystem() {}
};
