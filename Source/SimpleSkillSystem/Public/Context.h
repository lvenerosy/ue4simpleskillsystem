/*
 * @license
 * Copyright 2020 Loïc Venerosy
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "SimpleSkillSystemCore.h"

#include "Effect.h"
#include "EffectComponent.h"
#include "System.h"

#include "Context.generated.h"

UCLASS(BlueprintType)
	class SIMPLESKILLSYSTEM_API UContext final : public UObject
{
	GENERATED_BODY()

	struct Implementation;
	TUniquePtr<ImplementationDeleter> PointerToImplementation;

	Implementation* Concrete;

public:
	UContext();

	UFUNCTION(BlueprintCallable, Category = "Skill")
	UEffect* CreateEffect();

	bool IsRunning(const UEffect* Effect) const;

	void Launch(UEffect* Effect);

	UEffectComponent* AddQualifierPostStart(UEffect* Effect, TSubclassOf<UEffectComponent> NewQualifierType);

	void RemoveQualifierPostStart(UEffect* Effect, TSubclassOf<UEffectComponent> QualifierTypeToRemove);

	UFUNCTION(BlueprintCallable, Category = "Skill")
	void AddSystem(TSubclassOf<USystem> NewSystemType);

	// IMPORTANT: make sure this one is called before everything else in order to retrieve data relevant to the current frame
	// for example call it inside your game mode and make sure that your base character/actor tick after by using `AddTickPreRequisiteActor`
	UFUNCTION(BlueprintCallable, Category = "Skill")
	void Tick(float DeltaTime);

	bool IsWithinSystems() const;

	USystem::BufferedEffect* GetCorrespondingBuffered(const UEffect* Effect) const;

	UFUNCTION(BlueprintCallable, Category = "Skill")
	TArray<UEffect*> Query(const UObject* RequestSender) const;
};
