/*
 * @license
 * Copyright 2020 Loïc Venerosy
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "SimpleSkillSystemCore.h"

#include "EffectComponent.h"

#include "Effect.generated.h"

UCLASS(BlueprintType)
	class SIMPLESKILLSYSTEM_API UEffect final : public UObject
{
	GENERATED_BODY()

	struct Implementation;
	TUniquePtr<ImplementationDeleter> PointerToImplementation;

	Implementation* Concrete;

public:
	UEffect();

	UFUNCTION(BlueprintCallable, Category = "Skill")
	bool IsRunning() const;

	UEffectComponent* UnsafelyAddQualifier(TSubclassOf<UEffectComponent> NewQualifierType);

	// returns the write only version of the component so you can modify it
	// add a qualifier before `UEffect::Start` has been called, there is a post start version
	UFUNCTION(BlueprintCallable, Category = "Skill", meta = (DeterminesOutputType = "NewQualifierType"))
	UEffectComponent* AddQualifierPreStart(TSubclassOf<UEffectComponent> NewQualifierType);

	// returns the write only version of the component so you can modify it
	// add a qualifier after `UEffect::Start` has been called, there is a pre start version
	// cannot be called from the `USystem::Run`
	UFUNCTION(BlueprintCallable, Category = "Skill", meta = (DeterminesOutputType = "NewQualifierType"))
	UEffectComponent* AddQualifierPostStart(TSubclassOf<UEffectComponent> NewQualifierType);

	void UnsafelyRemoveQualifier(TSubclassOf<UEffectComponent> QualifierTypeToRemove);

	// remove a qualifier before `UEffect::Start` has been called, there is a post start version
	UFUNCTION(BlueprintCallable, Category = "Skill")
	void RemoveQualifierPreStart(TSubclassOf<UEffectComponent> QualifierTypeToRemove);

	// remove a qualifier after `UEffect::Start` has been called, there is a pre start version
	// cannot be called from the `USystem::Run`
	UFUNCTION(BlueprintCallable, Category = "Skill")
	void RemoveQualifierPostStart(TSubclassOf<UEffectComponent> QualifierTypeToRemove);

	UFUNCTION(BlueprintCallable, Category = "Skill")
	bool Matches(const TSet<TSubclassOf<UEffectComponent>>& SystemQualifiersIds) const;

	UFUNCTION(BlueprintCallable, Category = "Skill")
	TMap<TSubclassOf<UEffectComponent>, UEffectComponent*> GetMatching(const TSet<TSubclassOf<UEffectComponent>>& SystemQualifiersIds) const;

	void CopyQualifiers(const UEffect* Source);
	void CopyProperties(const UEffect* Source);

	// The ECS follows CQS where the `RequestSender` issues commands (effects) and the context returns responses
	// so ideally the systems should be pure outside of modifying the write only components then the sender manages the logic that uses the new data
	// this promotes a single source of truth.
	// `DeltaTimeSlack` is useful when you want to ensure frame perfectness
	// for example `SystemTryToStartNext` uses it in order to make sure that the next effect starts at the point where it should be,
	// as if it was running right from when the previous ended, instead of starting from the beginning.
	UFUNCTION(BlueprintCallable, Category = "Skill")
	void Start(UObject* RequestSender, const float DeltaTimeSlack = 0.f);

	void StartNext(UEffect* Previous, const float DeltaTimeSlack = 0.f);

	const UObject* RequestSender() const;

	bool IsTerminated() const;
	void Terminate();
};
